Describes how the algorithm performs as the input numbers grow and how they effect run time or space requirements.

- Big O notations:
	- Not a exact measurement just a concept.

### Example function with O(N) time complexity

```ts
function hello(n: string): number {
let sum = 0;
for (let i = 0; i < n.length; ++i) {
	sum += n.charCodeAt(i);
	}
// this shoud cause the runtime to be O(2N) but constants are dropped so still O(N)
for (let i = 0; i < n.length; ++i) {
	sum += n.charCodeAt(i);
	}

	return sum;
}
```

> *Tips: look for loops*

**explanation:** the for loop has to execute the length of string (aka number of inputs) times so if string length is proportionate to times the loop is executed so O(N). grows linear.

**Reasons for dropping constants:** 
	N = 1; O($10N$) = 1, O($N^2$) = 1
	N  = 5; O($10N$) = 50, O($N^2$) = 25
	N = 1000; O($10N$) = 10,000, O($N^2$) = 1,000,000 *// consts not making any senses after some huge numbers*

### Important concepts:
1. growth is with respect to the input
2. Consts are dropped
3. Worst case scenario is the way we measure (*usually*)

![[bigOgraph.png.png]]

> O($2^n$) & O($n!$) can't be run on current hardware so quite impractical. 

### Examples:

#### O($n^2$):
```go
package main

import "fmt"

func exampleON2(arr []int) {
    n := len(arr)
    // This loop runs 'n' times.
    for i := 0; i < n; i++ { 
		// Within the first loop, this loop also runs 'n' times.
        for j := 0; j < n; j++ { 
            // Some operation (e.g., printing or a simple computation)
            fmt.Println(arr[i], arr[j])
        }
    }
}

func main() {
    arr := []int{1, 2, 3, 4}
    exampleON2(arr)
}
```

#### O($n log n$):
- Quicksort
#### O($logn$):
- Binary search tree

#### O($\sqrt{n}$) -> will see later


next chap [[Chap 2 (Arrays)]]